FROM node:8-alpine
RUN apk update && apk add git nano
COPY / /pizza
RUN cd /pizza && npm install
CMD sh
