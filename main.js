var express = require('express');
var requests = require('request')
var url = require("url")

var app = express();

var bodyParser = require('body-parser')
app.use( bodyParser.json() );       // to support JSON-encoded bodies
app.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: false
})); 

app.get("/pizza", (req, res) => {
	res.send('<h1>PizzaRouter</h1><form action="/"><input type="text" name="url"/><input type="submit"/></form>');
});

var host = 'google.com'

app.get("/*", (req, res) => {
	var options = {
		url: (req.query.url ? req.query.url : ('http://' + host + req.originalUrl)),
		headers: {
			"User-Agent": "Mozilla/5.0 (X11; CrOS x86_64 9202.64.0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.146 Safari/537.36"
		}
	};
	if (req.query.url) host = new url.URL(req.query.url).hostname
	requests.get(options, function(error, response, body){
		res.send(body);
	});
});

app.listen(80)
